//Azadeh Ahmadi(1811386)
package inheritance;

public class Book {
	protected String title;
	private String author;
	public String getTitle(){return this.title;}
	public String getAuthor() {return this.author;}
	
	
	public Book (String title,String author) {
		this.title=title;
		this.author=author;
	}
	@Override
	public String toString() {
		String s="title:"+this.title;
		s=s+",author:"+this.author;
		return s;
	}

}
