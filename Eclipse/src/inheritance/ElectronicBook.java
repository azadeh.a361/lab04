//Azadeh Ahmadi(1811386)
package inheritance;

public class ElectronicBook extends Book {
	private int numberBytes;
	
	public ElectronicBook(String title,String author,int numberBytes) {
		super(title,author);
		this.numberBytes=numberBytes;
	}
	@Override
	public String toString() {
		String s=super.toString();
		s=s+",number of bytes:"+this.numberBytes;
		return s;
	}

}
