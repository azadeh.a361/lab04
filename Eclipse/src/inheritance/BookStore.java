//Azadeh Ahmadi(1811386)

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[]books=new Book[5];
		books[0]=new Book("Wuthering Heights","Emily Bronte");
		books[2]=new Book("Heart of Darkness", "Joseph Conrad");
		books[1]=new ElectronicBook("Madame Bovary","Gustave Flaubert",5876);
				books[3]=new ElectronicBook("In Search of Lost Time","Marcel Proust",9421);
						books[4]=new ElectronicBook("War and Peace","Leo Tolstoy",8971);
						
						
						for (int i=0; i<books.length;i++) {
							System.out.println(books[i].toString());
						}
	}

}
