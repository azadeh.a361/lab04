//azadeh Ahmadi(1811386)
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[]shapes=new Shape[5];
		shapes[0]=new Rectangle(4.0,8.0);
		shapes[1]=new Rectangle(8.0,7.5);
		shapes[2]=new Circle(4.0);
		shapes[3]=new Circle(9.0);
		shapes[4]=new Rectangle(7.0,7.0);
		
for (int i=0; i<shapes.length ;i++)
{System.out.println("Perimeter is: "+shapes[i].getPerimeter()+
		" Area is: "+shapes[i].getArea());
	}
	}

}
