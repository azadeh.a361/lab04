//Azadeh Ahmadi(1811386)
package geometry;

public interface Shape {
	double	getArea() ;
	double	getPerimeter(); 

}
